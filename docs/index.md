---
hide:
  - navigation
  - toc
  - footer
---

![Laurent Abbal](https://laurentabbal.forge.apps.education.fr/assets/images/id.png){ width="128" .middle align=left }
<br />
**Laurent Abbal**<br />
Professeur de NSI / Physique-Chimie<br />
Référent Numérique<br />
Lycée Français International de Tokyo<br /><br />
[:fontawesome-solid-paper-plane:{ .lien }](https://www.ateliernumerique.net)
[:fontawesome-brands-gitlab:{ .gitlab }](https://forge.apps.education.fr/laurent.abbal)
[:fontawesome-brands-github:{ .github }](https://github.com/laurentabbal)
[:fontawesome-brands-mastodon:{ .mastodon }](https://mastodon.social/@laurentabbal)
[:fontawesome-brands-twitter:{ .twitter }](https://twitter.com/laurentabbal)

<br />
<br />

#PROJETS

<div class="grid cards" markdown>

-   ![mon-oral.net](https://laurentabbal.forge.apps.education.fr/assets/images/favicon_mon-oraldotnet.png){ width="32" .middle } __mon-oral.net__

    ---

    <center>
    [:fontawesome-solid-paper-plane:{ .lien }](https://www.mon-oral.net)
    [:fontawesome-brands-gitlab:{ .gitlab }](https://forge.apps.education.fr/mon-oral)
    [:fontawesome-brands-github:{ .github }](https://github.com/mon-oral)
    [:fontawesome-brands-twitter:{ .twitter }](https://twitter.com/mon_oral)
    </center>    

    Pratique de l'oral au primaire et au secondaire, préparation aux épreuves orales de collège et de lycée & création de commentaires audio pour les élèves.


-   ![Nuit du Code](https://laurentabbal.forge.apps.education.fr/assets/images/favicon_nuitducodedotnet.svg){ width="32" .middle } __Nuit du Code__

    ---

    <center>
    [:fontawesome-solid-paper-plane:{ .lien }](https://www.nuitducode.net)
    [:fontawesome-brands-gitlab:{ .gitlab }](https://forge.apps.education.fr/nuit-du-code)
    [:fontawesome-brands-github:{ .github }](https://github.com/nuitducode)
    [:fontawesome-brands-mastodon:{ .mastodon }](https://mastodon.social/@nuitducode)
    [:fontawesome-brands-twitter:{ .twitter }](https://twitter.com/nuitducode)
    </center>

    La Nuit du Code est un marathon de programmation durant lequel les élèves, par équipes de deux ou trois, ont 6h pour coder un jeu avec Scratch ou Python/Pyxel.

    
-   ![Code Puzzle](https://laurentabbal.forge.apps.education.fr/assets/images/favicon_codepuzzledotio.png){ width="32" .middle } __Code Puzzle__

    ---

    <center>
    [:fontawesome-solid-paper-plane:{ .lien }](https://www.codepuzzle.io)
    [:fontawesome-brands-gitlab:{ .gitlab }](https://forge.apps.education.fr/code-puzzle)
    [:fontawesome-brands-github:{ .github }](https://github.com/codepuzzle-io)
    [:fontawesome-brands-mastodon:{ .mastodon }](https://mastodon.social/@codepuzzle)
    [:fontawesome-brands-twitter:{ .twitter }](https://twitter.com/codepuzzleio)
    </center>    

    Puzzles de Parsons, défis avec tests à valider et devoirs supervisés pour apprendre Python au collège/lycée et s'entraîner  pour les épreuves de NSI.


-   ![Pyxel Studio](https://laurentabbal.forge.apps.education.fr/assets/images/favicon_pyxelstudiodotnet.png){ width="32" .middle } __Pyxel Studio__

    ---

    <center>
    [:fontawesome-solid-paper-plane:{ .lien }](https://www.pyxelstudio.net)
    [:fontawesome-brands-gitlab:{ .gitlab }](https://forge.apps.education.fr/pyxel-studio)
    [:fontawesome-brands-github:{ .github }](https://github.com/pyxel-studio)
    [:fontawesome-brands-mastodon:{ .mastodon }](https://mastodon.social/@pyxelstudio)
    [:fontawesome-brands-twitter:{ .twitter }](https://twitter.com/pyxel_studio)
    </center>    

    Environnement de développement Python/Pyxel en ligne pour créer et partager des jeux "retro-games 8 bits". Utile pour s'entraîner pour la Nuit du Code.


-   ![Edupyter](https://laurentabbal.forge.apps.education.fr/assets/images/favicon_edupyterdotnet.png){ width="32" .middle } __Edupyter__

    ---

    <center>
    [:fontawesome-solid-paper-plane:{ .lien }](https://www.edupyter.net)
    [:fontawesome-brands-gitlab:{ .gitlab }](https://forge.apps.education.fr/edupyter)
    [:fontawesome-brands-github:{ .github }](https://github.com/edupyter)
    [:fontawesome-brands-twitter:{ .twitter }](https://twitter.com/edupyter)
    </center>    

    Environnement de développement pour l'enseignement et l'apprentissage de Python. Edupyter peut être installé sur un disque dur ou une clé USB sans droits administrateur.


-   ![Cahiers Numériques](https://laurentabbal.forge.apps.education.fr/assets/images/favicon_cahiernumdotnet.png){ width="32" .middle } __Cahiers Numériques__

    ---

    <center>
    [:fontawesome-solid-paper-plane:{ .lien }](https://www.cahiernum.net)
    [:fontawesome-brands-gitlab:{ .gitlab }](https://forge.apps.education.fr/cahiers-numeriques)
    [:fontawesome-brands-github:{ .github }](https://github.com/cahiers-numeriques)
    [:fontawesome-brands-twitter:{ .twitter }](https://twitter.com/cahiernum)
    </center>    

    Un cahier numérique permet d'avoir, côte à côte, un document de travail (cours, tutoriel, exercices...) et un environnement de création (Scratch, Geogebra, Basthon, BlocksCAD...).


-   ![Texte Puzzle](https://laurentabbal.forge.apps.education.fr/assets/images/favicon_txtpuzzledotnet.png){ width="32" .middle } __Texte Puzzle__

    ---

    <center>
    [:fontawesome-solid-paper-plane:{ .lien }](https://www.txtpuzzle.net)
    [:fontawesome-brands-gitlab:{ .gitlab }](https://forge.apps.education.fr/txtpuzzle)
    [:fontawesome-brands-github:{ .github }](https://github.com/txtpuzzle)
    [:fontawesome-brands-twitter:{ .twitter }](https://twitter.com/txtpuzzle)
    </center>    

    Générateur et gestionnaire de "Textes Puzzles" (textes à trous, choix multiples, lignes mélangées) pour le français, les langues, les langues anciennes...


-   ![Dōzo](https://laurentabbal.forge.apps.education.fr/assets/images/favicon_dozodotapp.png){ width="32" .middle } __Dōzo__

    ---

    <center>
    [:fontawesome-solid-paper-plane:{ .lien }](https://www.dozo.app)
    [:fontawesome-brands-gitlab:{ .gitlab }](https://forge.apps.education.fr/dozo)
    [:fontawesome-brands-github:{ .github }](https://github.com/dozo-app)
    [:fontawesome-brands-twitter:{ .twitter }](https://twitter.com/dozoapp)
    </center>    

    Suivi de l'activité des élèves sur leur poste informatique lors de travaux en classe ou de devoirs.


-   ![Organisation d'examens](https://laurentabbal.forge.apps.education.fr/assets/images/favicon_organisation-examens.png){ width="32" .middle } __Organisation d'examens__

    ---

    <center>
    [:fontawesome-solid-paper-plane:{ .lien }](https://www.ateliernumerique.net/organisation-examens)
    [:fontawesome-brands-gitlab:{ .gitlab }](https://forge.apps.education.fr/laurent.abbal/organisation-examens)
    </center>    

    Optimisation des créneaux en fonction de la répartition des matières des élèves & création automatique des listes.

</div>



<br /><br /><br /><br /><br />
